﻿using SalonAutomobila.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalonAutomobila.Controllers
{
    public class ManufacturerController : Controller
    {
        public SalonContext db { get; set; } = new SalonContext();

        // GET: Manufacturer
        public ActionResult Index()
        {
            var manufacturers = db.Manufacturers.ToList();
            return View(manufacturers);
        }

        // GET: Manufacturer/Details/5
        public ActionResult Details(int id)
        {
            var manufacturer = db.Manufacturers.FirstOrDefault(m => m.Id == id);
            return View(manufacturer);
        }

        // GET: Manufacturer/Create
        public ActionResult Create()
        {
            Manufacturer manufacturer = new Manufacturer();
            return View(manufacturer);
        }

        // POST: Manufacturer/Create
        [HttpPost]
        public ActionResult Create(Manufacturer manufacturer)
        {
            try
            {
                db.Manufacturers.Add(manufacturer);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(manufacturer);
            }
        }

        // GET: Manufacturer/Edit/5
        public ActionResult Edit(int id)
        {
            var manufacturer = db.Manufacturers.FirstOrDefault(m => m.Id == id);
            return View(manufacturer);
        }

        // POST: Manufacturer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Manufacturer manufacturer)
        {
            try
            {
                db.Entry(manufacturer).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(manufacturer);
            }
        }

        // GET: ProductCategory/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ProductCategory/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var manufacturer = db.Manufacturers.Find(id);
                db.Manufacturers.Remove(manufacturer);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                var manufacturer = db.Manufacturers.Find(id);
                return View(manufacturer);
            }
        }
    }
}
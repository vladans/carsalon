﻿using SalonAutomobila.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalonAutomobila.Controllers
{
    public class SalonController : Controller
    {
        public SalonContext db { get; set; } = new SalonContext();

        // GET: Salon
        public ActionResult Index()
        {
            return View();
        }
    }
}
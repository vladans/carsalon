namespace SalonAutomobila.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Gotovmodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(),
                        Year = c.Int(nullable: false),
                        Cubic = c.Int(nullable: false),
                        Color = c.String(),
                        ManufacturerId = c.Int(nullable: false),
                        SalonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Manufacturers", t => t.ManufacturerId, cascadeDelete: true)
                .ForeignKey("dbo.Salons", t => t.SalonId, cascadeDelete: true)
                .Index(t => t.ManufacturerId)
                .Index(t => t.SalonId);
            
            CreateTable(
                "dbo.Manufacturers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Country = c.String(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalonId = c.Int(nullable: false),
                        ManufacturerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Manufacturers", t => t.ManufacturerId, cascadeDelete: true)
                .ForeignKey("dbo.Salons", t => t.SalonId, cascadeDelete: true)
                .Index(t => t.SalonId)
                .Index(t => t.ManufacturerId);
            
            CreateTable(
                "dbo.Salons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PIB = c.Int(nullable: false),
                        Name = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        Adress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contracts", "SalonId", "dbo.Salons");
            DropForeignKey("dbo.Cars", "SalonId", "dbo.Salons");
            DropForeignKey("dbo.Contracts", "ManufacturerId", "dbo.Manufacturers");
            DropForeignKey("dbo.Cars", "ManufacturerId", "dbo.Manufacturers");
            DropIndex("dbo.Contracts", new[] { "ManufacturerId" });
            DropIndex("dbo.Contracts", new[] { "SalonId" });
            DropIndex("dbo.Cars", new[] { "SalonId" });
            DropIndex("dbo.Cars", new[] { "ManufacturerId" });
            DropTable("dbo.Salons");
            DropTable("dbo.Contracts");
            DropTable("dbo.Manufacturers");
            DropTable("dbo.Cars");
        }
    }
}

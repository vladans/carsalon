﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    // Proizvođač automobila ima svoj naziv, drzavu, grad.
    public class Manufacturer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }

        //public List<Car> Cars { get; set; }
        //public List<Contract> Contracts { get; set; }
    }
}
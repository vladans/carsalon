﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    // Salon automobila ima svoj PIB, naziv, drzavu, grad i adresu.
    public class Salon
    {
        public int Id { get; set; }
        public int PIB { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Adress { get; set; }

        public List<Car> Cars { get; set; } = new List<Car>();
        //public List<Contract> Contracts { get; set; }
    }
}
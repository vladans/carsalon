﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    public class SalonContext : DbContext
    {
        public SalonContext() : base("name=CarSalonConnection")
        {
        }
        //1 - enable-migrations --> (u Package Manager Console-u)
        //Ako je doslo do greske prilikom migracije, ponoviti sa enable-migrations -f

        public DbSet<Car> Cars { get; set; }
        public DbSet<Salon> Salons { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Contract> Constracts { get; set; }
        // 2 - add-migration "ZadajImeMigracije"
        // 3 - Iskljuciti --> cascadeDelete: true
        // 4 - update-database
        // update-database -targetmigration:"ImeMigracije" --> Ako zelimo da se vratimo na neku migraciju
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    // Automobil sadrži podatke o modelu, godini proizvodnje, kubikaži i boji.
    public class Car
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public int Cubic { get; set; }
        public string Color { get; set; }

        //[ForeignKey("ManufacturerId")]
        public int ManufacturerId { get; set; }
        public Manufacturer Manufacturer { get; set; }

        //[ForeignKey("SalonId")]
        public int SalonId { get; set; }
        public virtual Salon Salon { get; set; }
    }
}
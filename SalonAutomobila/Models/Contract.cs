﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    public class Contract
    {
        public int Id { get; set; }

        public int SalonId { get; set; }
        public Salon Salon { get; set; }

        public int ManufacturerId { get; set; }
        public Manufacturer Manufacturer { get; set; }

    }
}